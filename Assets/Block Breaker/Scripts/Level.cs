﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class Level : MonoBehaviour
{
    public static Level instance;

    // parameters
    [Header("Level")]
    [SerializeField] int livesPoint;
    [SerializeField] TextMeshProUGUI livesPointText;
    [SerializeField] KrackoutBall ball;
    [SerializeField] float delayResetBall = 1f;
    [SerializeField] GameObject gameOverPanel;

    [Header("Points Earned")]
    [SerializeField] int breakableBlocks = 0;  // Serialized for debugging purposes

    // cached reference
    SceneLoader sceneLoader;

    // state
    public bool isGameOver = false;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else Destroy(gameObject);
    }

    private void Start()
    {
        sceneLoader = GetComponent<SceneLoader>();
        livesPointText.text = livesPoint.ToString();
        gameOverPanel.SetActive(false);
    }

    public void CountBlocks()
    {
        breakableBlocks++;
    }

    public void BlockDestroyed()
    {
        breakableBlocks--;
        if (breakableBlocks <= 0)
        {
            sceneLoader.LoadNextScene();
        }
    }

    public void HandleLoseCollider()
    {
        livesPoint--;
        if (livesPoint <= 0)
        {
            Debug.Log("Lose");
            isGameOver = true;
            gameOverPanel.SetActive(true);
        }
        else
        {
            // Reset ball to default paddle position
            Invoke(GetFunctionName(ResetBall), delayResetBall);
        }

        livesPointText.text = livesPoint.ToString();
    }

    private void ResetBall()
    {
        ball.isReset = true;
    }

    private static string GetFunctionName(Action method)
    {
        return method.Method.Name;
    }
}
