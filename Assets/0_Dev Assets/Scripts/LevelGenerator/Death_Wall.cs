using UnityEngine;

public class Death_Wall : MonoBehaviour
{
    public GameObject gameOverPanel;
    public GameObject inputScore;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "ball") 
        {
           FindObjectOfType<Game_Manager>().Miss();
        }
        if (collision.gameObject.name == "ball dupe" || collision.gameObject.name == "ball") 
        {
            FindObjectOfType<DupeBall>().DupeGone();
        }
    }

    public void GameOverPanel()
    {
        Time.timeScale = 0;
        gameOverPanel.SetActive(true);
    }

    public void NewGame()
    {
        FindObjectOfType<Game_Manager>().NewGame();
    }

    public void inputScorePopUp()
    {
        inputScore.SetActive(true);
    }
    
    /*void OnTriggerEnter(Collision2D collision)
    {
        gameObject.tag == "Ball"
    }*/

    /*void OnTriggerEnter(Collider TheColliderThatIWillBeCollidingWith)
    {
      if(Collider.gameObject.name == "ball dupe")
      {
        Destroy(TheColliderThatIWillBeCollidingWith.gameObject);
      }
    }*/

}
