using UnityEngine;

public class Level_Generator : MonoBehaviour
{

    public Texture2D map;

    public ColorToPrefab[] ColorMap;

    // Start is called before the first frame update
    void Start()
    {
        GenerateLevel();
    }

    void GenerateLevel()
    {
        for (int x = 0; x < map.width; x++)
        {
            for (int y = 0; y < map.height; y++)
            {
                GenerateTile(x, y);
            }
        }
    }

    void GenerateTile(int x, int y)
    {
        Color pixelColor = map.GetPixel(x, y);

        if (pixelColor.a == 0)
        {
            //ignore if transparent
            return;
        }
        foreach(ColorToPrefab ColorMap in ColorMap)
        {
            if (ColorMap.color.Equals(pixelColor))
            {
                Vector2 position = new Vector2(x, y);
                GameObject go = Instantiate(ColorMap.prefab, position, Quaternion.identity, transform);
                go.name = go.name.Replace("(Clone)", "");
            }
        }
    }

}
