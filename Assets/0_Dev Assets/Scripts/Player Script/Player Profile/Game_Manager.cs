using UnityEngine;
using UnityEngine.SceneManagement;
public class Game_Manager : MonoBehaviour
{
    //Script References
    public Ball_Bounce ball { get; private set; }
    public PaddleMove_Test paddle { get; private set; }
    public Obstacle[] obstacles { get; private set; }
    public ExtraLives[] ExtraLives { get; private set; }
    private PowerUpChecker PowerUpCheck;
    private BonusShip bonus;

    //Component References
    public AudioSource _audio;
    public AudioClip[] _audioClip;

    // Set UI Level, Score, Lives ada di scrip Score
    public static int level = 1;
    public static int score = 0;
    public static int lives = 3;
    public int hitAchivement = 0;
    public int currentScore;
    public int currentLevel;
    [SerializeField] string playerName;
    //[SerializeField] Score scoreHUD;

    //LOAD LEVEL
    private void Awake() // Untuk menload levelnya agar berfungsi
    {
        DontDestroyOnLoad(this.gameObject);
        SceneManager.sceneLoaded += OnLevelLoaded;
    }
    private void Start()
    {
        NewGame(); // Starting point saat memulai dari awal
    }

    //LOAD LEVEL

    //START GAME
    public void NewGame()
    {
        score = 0;
        lives = 3;
        level = 1;

        LoadLevel(1);
        Time.timeScale = 1;

    }
    public void LoadLevel(int level)
    {
        SceneManager.LoadScene("Level " + level);
    }

    private void OnLevelLoaded(Scene scene, LoadSceneMode mode)
    {
        this.ball = FindObjectOfType<Ball_Bounce>();
        this.paddle = FindObjectOfType<PaddleMove_Test>();
        this.obstacles = FindObjectsOfType<Obstacle>();
        this.ExtraLives = FindObjectsOfType<ExtraLives>();
        this.PowerUpCheck = FindObjectOfType<PowerUpChecker>();
        this.bonus = FindObjectOfType<BonusShip>();
    }
    private void ResetLevel() //Reset posisi bola dan paddle seperti awal
    {
        this.ball.ResetBall();
        this.paddle.ResetPaddle();
    }
    //START GAME


    //Level Mekanik

    public void Hit(Obstacle obstacle)
    {
        //score += obstacle.points; //menambahkan point

        if(!PowerUpCheck._multiplierActive)
        {
            Debug.Log("Score added");
            score += obstacle.points;
        }
        if(PowerUpCheck._multiplierActive)
        {
            //menambahkan double point
            score += obstacle.points * 2;
            Debug.Log("Score doubled");
        }

        this.hitAchivement += 1;
        if (this.hitAchivement > 0)
        {
            //FindObjectOfType<GlobalAchivement>().Achive();
        }
        if (Cleared())
        {
            Time.timeScale = 0;
            _audio.PlayOneShot(_audioClip[0]);
            FindObjectOfType<Complete>().CompleteLevel();
            FindObjectOfType<Timer>().stopTime();
            PlayerPrefs.SetInt("currentscore", score);
        }
    }

    public void OneUp(ExtraLives extralives)
    {
        lives += extralives.lives; //menambahkan lives
        score += extralives.scores;
    }

    /*public void ScoreMultiplier(ScoreMultiplier doublepoint)
    {
        //score += (doublepoint.DoubleScore) ? doublepoint.scores * 2 : doublepoint.scores;
    }*/

    private void GameOver()
    {
        FindObjectOfType<Death_Wall>().GameOverPanel();
        FindObjectOfType<Death_Wall>().inputScorePopUp();
        FindObjectOfType<Timer>().Finnish();
        _audio.PlayOneShot(_audioClip[1]);
        newHighScoreupdate();
        PlayerPrefs.SetInt("currentscore", score);
        
        //FindObjectOfType<SaveData>().SendScore();
        //FindObjectOfType<PlayerStats>().highscoreupdate();
        //HighScores.UploadScore(myName.text, currentScore);
        //LeaderBoardHandler.AddLeaderboardIfPossible (new LeaderBoardElement(playerName, Score.scoreHUD));
        //SceneManager.LoadScene("GameOver");
        //NewGame();
    }
    public void Miss() //Ketika kena Deathwall
    {
        lives--;

        if (lives > 0)
        {
            ResetLevel();
        }
        else
        {
            GameOver();
        }
    }
    //Level Mekanik

    public bool Cleared() //Kondisi Level apabila sudah tercapai
    {
        if (GameObject.FindWithTag("Breakable") == null)
        {Debug.Log("HI");
            return true;
        }
        else
        {
            return false;
        }
    }

    public void newHighScoreupdate()
    {
        Debug.Log("NewHighscore");
        currentScore = score;
        currentLevel = level;

        if (currentScore > PlayerPrefs.GetInt("highscore"))
        {
            PlayerPrefs.SetInt("highscore", currentScore);
            
        }

        if (currentLevel > PlayerPrefs.GetInt("highscorelevel"))
        {
            PlayerPrefs.SetInt("highscorelevel", currentLevel);
        
        }
    }

    private void Update()
    {
        //Debug.Log(score);
    }
}
