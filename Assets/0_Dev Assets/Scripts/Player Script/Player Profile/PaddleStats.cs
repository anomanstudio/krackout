using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleStats : MonoBehaviour
{
    public float SpeedStat; //default value
    private static PaddleStats StatsInstance;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        if (StatsInstance == null)
        {
            StatsInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        StatsInstance = FindObjectOfType<PaddleStats>();

        SpeedStat = 50;
    }
}
