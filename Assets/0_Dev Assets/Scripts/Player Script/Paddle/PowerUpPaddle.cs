using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpPaddle : MonoBehaviour
{
    //References
    private SpriteRenderer spriteRenderer;
    private PowerUpChecker _powerup;

    //Sprites
    public Sprite[] PaddleSprite;

    //Prefab Reference
    public MissileBullet prefab_missile;

    //Bool Power Up
    [SerializeField] private bool isDone = false;

    //floats
    [SerializeField] private float _canFire = 0f;
    [SerializeField] private float _fireRate = 0.5f;

    //Vector 3
    private Vector3 DefaultSize;

    private void Start()
    {
        DefaultSize = transform.localScale; //setting deafult size
    }

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        _powerup = FindObjectOfType<PowerUpChecker>();
    }

    private void Update()
    {
        if (_powerup._missileActive)
        {
            spriteRenderer.sprite = PaddleSprite[0];
            if (Input.GetKeyDown(KeyCode.Space) && Time.time > _canFire)
            {
                Shooting();
            }
            if(_powerup._glueActive == true)
            {
                _powerup._glueActive = false;
            }
        }

        if (_powerup._expandActive)
        {
            if (!isDone)
            {
                expandPaddle();
                isDone = true;
            }

        }
        else
        {
            isDone = false;
            transform.localScale = DefaultSize;
        }

        if (_powerup._glueActive)
        {
            spriteRenderer.sprite = PaddleSprite[1];
        }
        else
        {
            spriteRenderer.sprite = PaddleSprite[0];
        }
    }

    //POWER UP FUNCTION
    public void Shooting()
    {
        Debug.Log("Missile Launch");
        Instantiate(this.prefab_missile, this.transform.position, Quaternion.identity); //Spawn Missile
        _canFire = Time.time + _fireRate;
        Debug.Log("Shooting!");
    }

    public void expandPaddle()
    {
        transform.localScale += new Vector3(0.06f, 0, 0);
        spriteRenderer.sprite = PaddleSprite[2];
    }
}
