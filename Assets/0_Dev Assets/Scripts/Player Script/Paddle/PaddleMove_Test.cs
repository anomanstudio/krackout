using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleMove_Test : MonoBehaviour
{
    //Configs
    public new Rigidbody2D rigidbody { get; private set; }
    public Vector2 direction { get; private set; }
    public float speed;

    //Audio
    public AudioSource Audio;
    public AudioClip[] _audioClip;

    //script reference
    PowerUpChecker _powerUp;
    PaddleStats speedstats;

    //Paddle Condition
    bool IsFreezing = false;

    private void Awake()
    {
        this.rigidbody = GetComponent<Rigidbody2D>();
        _powerUp = FindObjectOfType<PowerUpChecker>();
        speedstats = FindObjectOfType<PaddleStats>();
        
    }

    private void Start()
    {
        
    }

    //Game Controller
    private void Update()
    {
        speed = speedstats.SpeedStat;
       
    }
    private void FixedUpdate()
    {
        //Mengerakkan Paddle
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            this.direction = Vector2.up;

        }
        else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            this.direction = Vector2.down;
        }
        else
        {
            this.direction = Vector2.zero;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            ResetPaddle();
        }

        if (this.direction != Vector2.zero)
        {
            this.rigidbody.AddForce(this.direction * this.speed);
        }
    }

    public void ResetPaddle()
    {
        this.transform.position = new Vector2( 40f , 9f);
        this.rigidbody.velocity = Vector2.zero;
        _powerUp.disableAll();
    }

    IEnumerator Freeze(int time)
    {
        RigidbodyConstraints2D previousConstraints = rigidbody.constraints;

        IsFreezing = true;
        rigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
        yield return new WaitForSeconds(time);
        IsFreezing = false;
        rigidbody.constraints = previousConstraints;
        
    }

    public void StartFreeze()
    {
        StartCoroutine(Freeze(3));
        Audio.PlayOneShot(_audioClip[0]);
    }
}

