using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DupeBall : MonoBehaviour
{
    //Basic needs
    private float speed = 900f;
    [SerializeField] private Transform attachment; //for attachment to glue
    private GameObject theObject;

    //Bool Power Up
    [SerializeField] private bool _isGlued = false;

    //ScriptReferences
    PowerUpChecker _powerUp;

    //Component references
    public new Rigidbody2D rigidbody { get; private set; }
    public AudioSource Audio;
    public AudioClip[] _audioClip;

    public void Awake()
    {
        this.rigidbody = GetComponent<Rigidbody2D>();
        _powerUp = FindObjectOfType<PowerUpChecker>();
        //theObject = GameObject.Find("GlueAttachment");
        //attachment = theObject.transform;
        theObject = GameObject.FindGameObjectWithTag("GlueAttachment");
        attachment = theObject.transform;
    }

    private void Start()
    {
        
    }

    private void OnEnable()
    {
        SetRandomTrejactory(); //this will launch the ball when instantiated
    }

    private void Update()
    {
        if (_isGlued == true)
        {
            GluePaddle();

            if (Input.GetKeyDown(KeyCode.Space))
            {
                UnGlue();
                SetRandomTrejactory();
                _isGlued = false;
            }
        }
    }

    //Collision condition ball
    private void OnCollisionEnter2D(Collision2D other)
    {
        Audio.PlayOneShot(_audioClip[0]);
        if (other.gameObject.tag == "Player" || other.gameObject.tag == "Breakable")
        {
            this.rigidbody.velocity *= 1.03f;
            Audio.PlayOneShot(_audioClip[0]);
        }

        if (other.gameObject.tag == "Enemy")
        {
            Audio.PlayOneShot(_audioClip[2]);
        }

        if (other.gameObject.tag == "Unbreakable")
        {
            Audio.PlayOneShot(_audioClip[3]);
        }

        if (other.gameObject.tag == "Power Up")
        {
            Audio.PlayOneShot(_audioClip[4]);
        }

        if(other.gameObject.tag == "Deathwall")
        {
            this.gameObject.SetActive(false);
        }
        if (other.gameObject.tag == "Player" && _powerUp._glueActive)
        {
            this.rigidbody.velocity = Vector2.zero;
            _isGlued = true;
            Debug.Log(attachment);
        }
    }

    public void SetRandomTrejactory()
    {
        Vector2 Force = Vector2.zero;
        Force.x = 1f;
        Force.y = Random.Range(-0.5f, 0.5f);

        this.rigidbody.AddForce(-Force.normalized * speed);
    }

    //Glue Condition
    public void GluePaddle()
    {
        this.gameObject.transform.parent = attachment;
        this.gameObject.transform.localPosition = Vector2.zero;
    }
    public void UnGlue()
    {
        this.gameObject.transform.parent = null;
    }

    //Destroy when ball miss
    public void DupeGone()
    {
        this.gameObject.SetActive(false);
    }
}
