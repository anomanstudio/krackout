using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball_Bounce : MonoBehaviour
{
    //Configs
    //Script References
    public PaddleMove_Test paddle;
    private PowerUpChecker powerup;
    //Component References
    public new Rigidbody2D rigidbody { get; private set; }
    public AudioSource Audio;
    public AudioClip[] _audioClip;
    //Floats
    public float speed = 500f;
    private float magnitude;
    //Vector
    Vector2 followpaddle;
    //bool
    [SerializeField] private bool GameStart = false;

    public int wallBounce;
    public float wallBounceTimer;

    public float velocity;

    public void Awake()
    {
        this.rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        //Ketika game mulai, bola akan mengikuti paddle
        //ResetBall();
        followpaddle = transform.position - paddle.transform.position;
        
    }

    private void Update()
    {
        velocity = this.rigidbody.velocity.magnitude;

        if (velocity <= 7)
        {
            Vector3 velocity = rigidbody.velocity;
            Vector3 direction = velocity.normalized;

            this.rigidbody.velocity = direction * magnitude * 1.1f;
        }
        if (velocity >= 40)
        {
            Vector3 velocity = rigidbody.velocity;
            Vector3 direction = velocity.normalized;

            this.rigidbody.velocity = direction * magnitude * 0.9f;
        }

        if (!GameStart) //to start the game press space
        {
            BallLocked();
            if (Input.GetKeyDown(KeyCode.Space))
            {
                BallUnlocked();
                FindObjectOfType<Timer>().resumeTime();
            }
            
        }
    }

    public void BallLocked()
    {
        Vector2 Paddlepos = new Vector2(paddle.transform.position.x, paddle.transform.position.y);
        transform.position = Paddlepos + followpaddle;
    }

    public void BallUnlocked()
    {
        GameStart = true; //Game is started
        SetRandomTrejactory();
        Audio.PlayOneShot(_audioClip[1]);
        //this.rigidbody.AddForce(Vector2.right * speed);

    }

    public void SetRandomTrejactory()
    {
        Vector2 Force = Vector2.zero;
        Force.x = 1f;
        Force.y = Random.Range(-0.5f, 0.5f);

        this.rigidbody.AddForce(-Force.normalized * speed);
    }

    public void ResetBall()
    {
        GameStart = false;
        BallLocked();
        this.rigidbody.velocity = Vector2.zero;
        //Invoke(nameof(SetRandomTrejactory), 1f);
        
    }

    public void LaunchBall()
    {
        //powerup.UnGlue();
        Vector2 Force = Vector2.zero;
        Force.x = 1f;
        //Force.y = Random.Range(-0.5f, 0.5f);
        this.rigidbody.AddForce(-Force.normalized * this.speed);
        //this will randomized ballspeed at every collision

    }

    //Collision condition ball
    private void OnCollisionEnter2D(Collision2D other)
    {
        Audio.PlayOneShot(_audioClip[0]);
        if (other.gameObject.tag == "Player")
        {
            magnitude = rigidbody.velocity.magnitude;

            //this.rigidbody.velocity *= 1.1f;
            Audio.PlayOneShot(_audioClip[0]);
            wallBounce += 1;
        }
        if(other.gameObject.tag == "Breakable")
        {
            Audio.PlayOneShot(_audioClip[0]);
        }
        
        if(other.gameObject.tag == "Enemy")
        {
            Audio.PlayOneShot(_audioClip[2]);

            wallBounce = 0;
        }

        if (other.gameObject.tag == "Unbreakable")
        {
            Audio.PlayOneShot(_audioClip[3]);

            wallBounce = 0;
        }

        if(other.gameObject.tag == "Power Up")
        {
            Audio.PlayOneShot(_audioClip[4]);

            wallBounce = 0; 
        }
        
        if(other.gameObject.tag == "Wall" && wallBounce < 10)
        {
            wallBounce += 1;
        }

        if (other.gameObject.tag == "Wall" && wallBounce >= 10) //eksekusi script untuk mengeluarkan dari loop vertikal
        {
            magnitude = rigidbody.velocity.magnitude;
            this.rigidbody.velocity = new Vector3(Random.Range(-1, -0.5f),Random.Range(-.3f, -.3f),0) * magnitude * 1.1f;

            wallBounce = 0; //stop script from looping
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        Vector3 velocity = rigidbody.velocity;
        Vector3 direction = velocity.normalized;

        if(other.gameObject.tag == "Player")
        {
            this.rigidbody.velocity = direction * magnitude * 1.1f;
        }
    }

    public void destroyBall()
    {
        Destroy(gameObject);
    }



}
