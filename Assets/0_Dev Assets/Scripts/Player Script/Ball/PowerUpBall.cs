using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpBall : MonoBehaviour
{
    //Sprites
    //Prefab Reference
    [SerializeField] private Transform attachment;

    //Bool Power Up
    [SerializeField] private bool _isGlued = false;

    //references
    private Rigidbody2D rb;
    Ball_Bounce ball;
    PowerUpChecker _powerUp;

    //floats
    [SerializeField]
    private float _timeScale = 1f;
    public float TimeScale
    {
        get { return _timeScale; }
        set
        {
            if (!_powerUp._Slowdownactive)
            {
                rb.velocity /= TimeScale;
            }

            _timeScale = Mathf.Abs(value);

            rb.velocity *= TimeScale;
        }
    }

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    private void Start()
    {
        ball = FindObjectOfType<Ball_Bounce>();
        _powerUp = FindObjectOfType<PowerUpChecker>();
    }

    private void Update()
    {
        if (_isGlued == true)
        {
            GluePaddle();

            if (Input.GetKeyDown(KeyCode.Space))
            {
                UnGlue();
                ball.LaunchBall();
                _isGlued = false;
            }
        }

        if (_powerUp._Slowdownactive)
        {
            SlowingDown();
        }
    }

    //POWER UP FUNCTIONS
    public void GluePaddle()
    {
        this.gameObject.transform.parent = attachment;
        this.gameObject.transform.localPosition = Vector2.zero;
    }

    public void UnGlue()
    {
        this.gameObject.transform.parent = null;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player" && _powerUp._glueActive)
        {
            this.rb.velocity = Vector2.zero;
            _isGlued = true;
        }
    }
    public void SlowingDown()
    {
        TimeScale = 0.99f;
        StartCoroutine(SlowDownOff(0.2f));
    }

    IEnumerator SlowDownOff(float timer)
    {
        yield return new WaitForSeconds(timer);
        _powerUp._Slowdownactive = false;
    }

}
