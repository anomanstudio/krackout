using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeHive : MonoBehaviour
{
    public EnemyBee PrefabBee;
    private float _canSpawn = 0;
    public float _spawnRate = 2f;

    private void Update()
    {
        if(Time.time > _canSpawn)
        {
            SpawnBees();
        }
    }

    //Collision
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Ball")
        {
            //Hide Object
            gameObject.SetActive(false);
        }
    }

    //Spawn Function
    private void SpawnBees()
    {
        Instantiate(this.PrefabBee, this.transform.position, Quaternion.identity); //Spawn Bee
        _canSpawn = Time.time + _spawnRate;
    }
}
