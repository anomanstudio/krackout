using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawn : MonoBehaviour
{
    private Rigidbody2D rb;
    private float dirX, dirY, moveSpeed;

    [SerializeField]
    private GameObject BeeHive;
    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        //moveSpeed = 5f;
        StartCoroutine(Spawn());
    }

    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(dirX, dirY);
    }

    private void SpawnBeeHive()
    {
        bool BeeHiveSpawned = false;
        while (!BeeHiveSpawned)
        {
            Vector3 BeeHivePosition = new Vector3(Random.Range(4f,12f), Random.Range(16f, 4f), 0f);
            if ((BeeHivePosition - transform.position).magnitude < 3)
            {
                continue;
            }
            else
            {
                Instantiate(BeeHive, BeeHivePosition, Quaternion.identity);
                BeeHiveSpawned = true;
            }
        }
    }

    IEnumerator Spawn()
    {
        while(true)
        {
            yield return new WaitForSeconds(30);
            SpawnBeeHive();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Apabila object Ball dan Missile collide dengan Obstacle
        if (collision.gameObject.tag == "Ball")
        {
            this.gameObject.SetActive(false);
        }

    }
}
