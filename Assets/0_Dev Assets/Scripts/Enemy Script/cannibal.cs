using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cannibal : MonoBehaviour
{
    public Vector3 targetPos;
    public float speed;

    private void Update()
    {
        this.transform.position += this.targetPos * this.speed * Time.deltaTime;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ball") 
        {
           //gameObject.SetActive(false);
            FindObjectOfType<Game_Manager>().Miss();
        }

        if (collision.gameObject.tag == "Wall")
        {
            gameObject.SetActive(false);
        }

        if (collision.gameObject.tag == "Player")
        {
            FindObjectOfType<PaddleMove_Test>().StartFreeze();
        }
    }
}
