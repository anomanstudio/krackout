using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lightning : MonoBehaviour
{
    public Vector2 fieldOfImpact;
    //public float force;
    public LayerMask LayerToHit;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ball" || collision.gameObject.tag == "Missile") 
        {
            explode();
            this.gameObject.SetActive(false);
        }
    }

    //testing
    /*private void Update()
    {
        if (Input.GetKey(KeyCode.B))
        {
            explode();
            //this.gameObject.SetActive(false);
        }
    }*/

    void explode()
    {
        Collider2D[] hitCollider = Physics2D.OverlapBoxAll(transform.position, fieldOfImpact, 0, LayerToHit);

        for(int i = 0; i < hitCollider.Length; i++)
        {
            if (hitCollider[i].gameObject.tag == "Breakable")
            {
                //hitCollider[i].gameObject.SetActive(false);
                hitCollider[i].GetComponent<Obstacle>().Hit();
            }

        }
    }
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawCube(transform.position,new Vector3 (fieldOfImpact.x, fieldOfImpact.y, 0));
    }

}
