using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Double : MonoBehaviour
{
    public PaddleMove_Test paddle;
    private Vector2 followposition;
    private Rigidbody2D rb;
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        FollowPaddle();
    }
    private void Start()
    {
        followposition = transform.position - paddle.transform.position;
    }
    public void FollowPaddle()
    {
        Vector2 Paddlepos = new Vector2(paddle.transform.position.x, paddle.transform.position.y);
        transform.position = Paddlepos + followposition;
    }
}
