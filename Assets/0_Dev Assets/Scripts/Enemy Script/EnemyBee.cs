using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBee : MonoBehaviour
{
    public Vector3 targetPos;
    public float speed = 30f;

    void Update()
    {
        this.transform.position += this.targetPos * this.speed * Time.deltaTime;
    }
    
     private void OnCollisionEnter2D(Collision2D collision)
    {
        //Apabila object Ball dan Missile collide dengan Obstacle
        if (collision.gameObject.tag == "Ball")
        {
            this.gameObject.SetActive(false);
        }

         if (collision.gameObject.tag == "Player")
        {
            this.gameObject.SetActive(false);
            FindObjectOfType<PaddleMove_Test>().StartFreeze();
        }

    }
    
}
