using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cannibalSpawn : MonoBehaviour
{
    public cannibal prefab_cannibal;
    private float _canSpawn = 0;
    public float _spawnRate = 10f;

    private void Update()
    {
        if (Time.time > _canSpawn)
        {
            SpawnBees();
        }
    }

    //Spawn Function
    private void SpawnBees()
    {
        Instantiate(this.prefab_cannibal, this.transform.position, Quaternion.identity); //Spawn Bee
        _canSpawn = Time.time + _spawnRate;
    }
}
