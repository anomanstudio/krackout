using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningSpawn : MonoBehaviour
{
    private Rigidbody2D rb;
    //private float dirX, dirY, moveSpeed;

    [SerializeField]
    private GameObject lightning;
    
    void Start()
    {
        //rb = GetComponent<Rigidbody2D>();
        //moveSpeed = 5f;
        StartCoroutine(Spawn());
    }

    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        //rb.velocity = new Vector2(dirX, dirY);
    }

    private void Spawnlightning()
    {
        bool lightningSpawned = false;
        while (!lightningSpawned)
        {
            Vector3 lightningPosition = new Vector3(Random.Range(4f,12f), Random.Range(16f, 4f), 0f);
            if ((lightningPosition - transform.position).magnitude < 3)
            {
                continue;
            }
            else
            {
                Instantiate(lightning, lightningPosition, Quaternion.identity);
                lightningSpawned = true;
            }
        }
    }

    IEnumerator Spawn()
    {
        while(true)
        {
            yield return new WaitForSeconds(300);
            Spawnlightning();
        }
    }
}
