using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusShipSpawn : MonoBehaviour
{
    private Rigidbody2D rb;
    //private float dirX, dirY, moveSpeed;

    [SerializeField]
    private GameObject BonusShip;
    
    void Start()
    {
        //rb = GetComponent<Rigidbody2D>();
        //moveSpeed = 5f;
        StartCoroutine(Spawn());
    }

    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        //rb.velocity = new Vector2(dirX, dirY);
    }

    private void SpawnBonusShip()
    {
        bool BonusShipSpawned = false;
        while (!BonusShipSpawned)
        {
            Vector3 BonusShipPosition = new Vector3(Random.Range(4f,12f), Random.Range(16f, 4f), 0f);
            if ((BonusShipPosition - transform.position).magnitude < 3)
            {
                continue;
            }
            else
            {
                Instantiate(BonusShip, BonusShipPosition, Quaternion.identity);
                BonusShipSpawned = true;
            }
        }
    }

    IEnumerator Spawn()
    {
        while(true)
        {
            yield return new WaitForSeconds(120);
            SpawnBonusShip();
        }
    }
}
