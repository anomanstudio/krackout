using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreMultiplier : MonoBehaviour
{
    public int health { get; private set; }
    public int scores = 100;
    private PowerUpChecker _powerup;

    private void Start()
    {
        //DoubleScore = false;
        _powerup = FindObjectOfType<PowerUpChecker>();
    }

    private void BrickState()
    {
        this.health--;
        if (this.health <= 0)
        {
            this.gameObject.SetActive(false);
        }
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ball" || collision.gameObject.tag == "Missile")
        {
            BrickState();
            _powerup._multiplierActive = true;

            //false other powerup
            _powerup._expandActive = false;
            _powerup._glueActive = false;
            _powerup._missileActive = false;
            _powerup._Slowdownactive = false;
            _powerup._ShieldActive = false;
        }
    }
}
