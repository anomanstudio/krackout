using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwinBall : MonoBehaviour
{
    public Rigidbody2D dupeBall;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ball") 
            {
                Instantiate(dupeBall, transform.position, transform.rotation);
                gameObject.SetActive(false);
            }
        }
    
}
