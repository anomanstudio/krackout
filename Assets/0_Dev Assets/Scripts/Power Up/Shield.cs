using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    private PowerUpChecker powerupcheck;
    public GameObject _shield;

    private void Awake()
    {
        powerupcheck = FindObjectOfType<PowerUpChecker>();
    }

    private void shieldSpawn()
    {
        Instantiate(this._shield, new Vector3(44.2982f, 10.6589f,0), Quaternion.identity); //Spawn shield
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ball" || collision.gameObject.tag == "Missile") 
        {
            gameObject.SetActive(false);
            powerupcheck._ShieldActive = true;
            shieldSpawn();
            

            //False other bool
            powerupcheck._missileActive = false;
            powerupcheck._glueActive = false;
            powerupcheck._expandActive = false;
            powerupcheck._multiplierActive = false;
            powerupcheck._Slowdownactive = false;
        }
    }

}
