using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ball" || collision.gameObject.tag == "Missile") 
        {
            foreach (var gameObj in GameObject.FindGameObjectsWithTag("Breakable"))
            {
            Destroy(gameObj);
            }

           //FindObjectOfType<Game_Manager>().Cleared();
           Cleared();
           gameObject.SetActive(false);
        }
    }
    public void Cleared()
    {
        /*Game_Manager.level += 1;
        SceneManager.LoadScene("Level " + Game_Manager.level);
        FindObjectOfType<PowerUpChecker>().disableAll();
        FindObjectOfType<Ball_Bounce>().ResetBall();
        FindObjectOfType<PaddleMove_Test>().ResetPaddle();*/
                    Time.timeScale = 0;
            FindObjectOfType<Complete>().CompleteLevel();
            FindObjectOfType<Timer>().stopTime();
    }
}
