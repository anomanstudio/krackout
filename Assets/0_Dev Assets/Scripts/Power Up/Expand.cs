using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Expand : MonoBehaviour
{
    PowerUpChecker _powerup;

    private void Start()
    {
        _powerup = FindObjectOfType<PowerUpChecker>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ball" || collision.gameObject.tag == "Missile") 
        {
            _powerup._expandActive = true;
            gameObject.SetActive(false);

            //false other power up
            _powerup._glueActive = false;
            _powerup._missileActive = false;
            _powerup._multiplierActive = false;
            _powerup._Slowdownactive = false;
            _powerup._ShieldActive = false;
        }
       
    }
}
