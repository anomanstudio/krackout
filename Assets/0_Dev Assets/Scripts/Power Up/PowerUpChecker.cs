using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpChecker : MonoBehaviour
{
    //POWER UP BOOLEAN
    public bool _glueActive = false;
    public bool _missileActive = false;
    public bool _expandActive = false;
    public bool _multiplierActive = false;
    public bool _Slowdownactive = false;
    public bool _ShieldActive = false;
    //REFERENCES

    //BOOL DEACTIVETOR
    public void disableAll()
    {
        _glueActive = false;
        _missileActive = false;
        _expandActive = false;
        _multiplierActive = false;
        _Slowdownactive = false;
        _ShieldActive = false;
    }

    private void Update()
    {
        
    }
}
