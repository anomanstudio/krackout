using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtraLives : MonoBehaviour
{
    public int health { get; private set; }
    public int scores = 50;
    public int lives = 1;

    private void Start()
    {
        
    }

    private void BrickState()
    {
        this.health--;
        if (this.health <= 0)
        {
            this.gameObject.SetActive(false);
        }
        FindObjectOfType<Game_Manager>().OneUp(this);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ball" || collision.gameObject.tag == "Missile")
        {
            Debug.Log("Terkena extra live");
            BrickState();
        }
    }
}
