using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shieldLine : MonoBehaviour
{
    private PowerUpChecker powerupcheck;

    private void Awake()
    {
        powerupcheck = FindObjectOfType<PowerUpChecker>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ball") 
        {
            gameObject.SetActive(false);
            powerupcheck._ShieldActive = false;
        }
    }

}
