using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    public float fieldOfImpact;
    //public float force;
    public LayerMask LayerToHit;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ball" || collision.gameObject.tag == "Missile") 
        {
            this.gameObject.SetActive(false);
            explode();
        }
    }

    //testing
    /*private void Update()
    {
        if (Input.GetKey(KeyCode.B))
        {
            explode();
            //this.gameObject.SetActive(false);
        }
    }*/

    void explode()
    {
        Collider2D[] hitCollider = Physics2D.OverlapCircleAll(transform.position, fieldOfImpact, LayerToHit);

        for(int i = 0; i < hitCollider.Length; i++)
        {
            if (hitCollider[i].gameObject.tag == "Breakable")
            {
                //hitCollider[i].gameObject.SetActive(false);
                hitCollider[i].GetComponent<Obstacle>().Hit();
            }

        }
    }
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position,fieldOfImpact);
    }
}
