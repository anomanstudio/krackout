using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Glue : MonoBehaviour
{
    //Script ini untuk brick power up slow down
    //Jika game object bola collide dengan power up ini
    PowerUpChecker _powerup;

    private void Start()
    {
        _powerup = FindObjectOfType<PowerUpChecker>();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        //Apabila collide dengan tag bola
        if (other.gameObject.tag == "Ball" || other.gameObject.tag == "Missile")
        {
            Debug.Log("Obtained Glue");
            _powerup._glueActive = true;
            gameObject.SetActive(false);

            //False object lain
            _powerup._expandActive = false;
            _powerup._missileActive = false;
            _powerup._multiplierActive = false;
            _powerup._Slowdownactive = false;
            _powerup._ShieldActive = false;
        }
    }
}
