using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour
{
    //Script ini untuk brick power up missile
    //Jika game object bola collide dengan power up ini
    //Panggil script Paddle
    PowerUpChecker _powerUp;

    private void Start()
    {
        _powerUp = FindObjectOfType<PowerUpChecker>();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        //Apabila collide dengan tag bola
        if (other.gameObject.tag == "Ball" || other.gameObject.tag == "Missile")
        {
            gameObject.SetActive(false);
            _powerUp._missileActive = true;

            //false other powerup
            _powerUp._glueActive = false;
            _powerUp._multiplierActive = false;
            _powerUp._expandActive = false;
            _powerUp._Slowdownactive = false;
            _powerUp._ShieldActive = false;
        }
    }
}
