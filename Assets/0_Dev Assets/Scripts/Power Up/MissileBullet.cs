using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileBullet : MonoBehaviour
{
    //Ini script untuk missile bullet
    public Vector3 direction;
    public float speed;
    //public System.Action missileinactive; //untuk info apakah missile sudah tidak aktif

    private void Update()
    {
        //Pergerakan missile
        this.transform.position += this.direction * this.speed * Time.deltaTime;
    }

    //Apabila missile mengenai object lain
    private void OnCollisionEnter2D(Collision2D other)
    {
        /*Apabila sudah mengenai objek, maka ini akan menginfokan ke event missileinactive
         * bahwa missile sudah tidak aktif
         */
        //this.missileinactive.Invoke();
        //Missile yg sudah terhit akan dihide
        gameObject.SetActive(false);
        Debug.Log("Hit Object");
    }
}
