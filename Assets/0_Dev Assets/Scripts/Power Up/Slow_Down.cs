using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slow_Down : MonoBehaviour
{
    //Script ini untuk brick power up slow down
    //Jika game object bola collide dengan power up ini
    private PowerUpChecker _powerup;

    private void Start()
    {
        _powerup = FindObjectOfType<PowerUpChecker>();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        //Apabila collide dengan tag bola
        if (other.gameObject.tag == "Ball" || other.gameObject.tag == "Missile")
        {
            Debug.Log("Slow down received");
            _powerup._Slowdownactive = true;
            gameObject.SetActive(false);

            //false other powerup
            _powerup._expandActive = false;
            _powerup._glueActive = false;
            _powerup._multiplierActive = false;
            _powerup._missileActive = false;
        }
    }
}
