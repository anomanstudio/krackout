using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Obstacle : MonoBehaviour
{
    public SpriteRenderer spriteRenderer { get; private set; }
    public Sprite[] states;

    //public ScoreMultiplier DoubleScore { get; private set; }
    public int health { get; private set; }
    public bool unbreakable;
    public int points = 100;

    //listing prefab yang akan dispawn
    public GameObject[] Prefabs;

    private void Awake()
    {
        this.spriteRenderer = GetComponent<SpriteRenderer>();
        //this.DoubleScore = FindObjectOfType<ScoreMultiplier>();
    }

    private void Start()
    {
        this.gameObject.SetActive(true);
        //Apabila brick adalah unbreakable
        if (!this.unbreakable)
        {
            this.health = this.states.Length;
            this.spriteRenderer.sprite = this.states[this.health - 1];
        }
        //ResetBrick();
    }

    //public void ResetBrick(){}

    //Brick when geting hit
    public void Hit()
    {
        if (this.unbreakable)
        {
            return;
        }

        this.health--;

        if (this.health <= 0)
        {
            this.gameObject.SetActive(false);
        }
        else
        {
            this.spriteRenderer.sprite = this.states[this.health - 1];
        }

        FindObjectOfType<Game_Manager>().Hit(this);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Apabila object Ball dan Missile collide dengan Obstacle
        if (collision.gameObject.tag == "Ball" || collision.gameObject.tag == "Missile" || collision.gameObject.tag == "BallDupe" )
        {
            //Sudah di hit bricknya
            Hit();

            //Random extra life after destroy chances
            int randchance = Random.Range(1, 101);
            if (randchance < 50)
            {
                //StartCoroutine(ItemDrop(1));
                SpawnItem();
            }

            GlobalAchivement.ach01Count += 1;
        }
    }

    private void SpawnItem()
    {
        int indextoDrop = Random.Range(0, Prefabs.Length);
        Instantiate(Prefabs[indextoDrop], transform.position, transform.rotation);
    }

    /*IEnumerator ItemDrop(int timer)
    {
        yield return new WaitForSeconds(timer);
        SpawnItem();
    }*/
}
