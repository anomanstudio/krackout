using System;

[Serializable]
public class LeaderBoardElement
{
    public string playerName;
    public int points;

    public LeaderBoardElement ( string name, int points)
    {
        playerName = name;
        this.points = points;
    }
}
