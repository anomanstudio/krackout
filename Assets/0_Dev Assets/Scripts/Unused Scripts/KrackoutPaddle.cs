using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KrackoutPaddle : MonoBehaviour
{
    // config params
    [SerializeField] float minY = 1f;
    [SerializeField] float maxY = 15f;
    [SerializeField] float screenHeightInUnits = 16f;

    // cached references
    GameSession theGameSession = null;
    KrackoutBall theBall;

    // Start is called before the first frame update
    void Start()
    {
        theGameSession = FindObjectOfType<GameSession>();
        theBall = FindObjectOfType<KrackoutBall>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Level.instance.isGameOver) { return; }
        Vector2 paddlePos = new Vector2(transform.position.x, transform.position.y);
        paddlePos.y = Mathf.Clamp(GetYPos(), minY, maxY);
        transform.position = paddlePos;

    }

    private float GetYPos()
    {
        if (theGameSession.IsAutoPlayEnabled())
        {
            return theBall.transform.position.y;
        }
        else //input control
        {
            return Input.mousePosition.y / Screen.height* screenHeightInUnits;
        }
    }
}
