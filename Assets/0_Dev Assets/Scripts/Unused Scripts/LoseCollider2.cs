using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseCollider2 : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        // Go To Level Handler of Lives Points?
        Level.instance.HandleLoseCollider();
    }
}
