using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaderBoardHandler : MonoBehaviour
{
    List<LeaderBoardElement> LeaderBoardList = new List<LeaderBoardElement>();
    [SerializeField] int maxCount = 5;
    [SerializeField] string filename;
    
    private void Start()
    {
        LoadLeaderboard();
    }

    private void LoadLeaderboard()
    {
        LeaderBoardList = FileHandler.ReadListFromJSON<LeaderBoardElement> (filename);

        while(LeaderBoardList.Count > maxCount)
        {
            LeaderBoardList.RemoveAt (maxCount);
        }
    }

    private void SaveLeaderboard()
    {
        FileHandler.SaveToJSON<LeaderBoardElement> (LeaderBoardList, filename);
    }

    public void AddLeaderboardIfPossible (LeaderBoardElement element)
    {
        for (int i = 0; i < maxCount; i++)
        {
            if (i >=LeaderBoardList.Count || element.points > LeaderBoardList[i].points)
            {
                LeaderBoardList.Insert (i, element);

                while (LeaderBoardList.Count > maxCount)
                {
                    LeaderBoardList.RemoveAt (maxCount);
                }

                SaveLeaderboard ();

                break;
            }
        }
    }
}
