using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleSprite : MonoBehaviour
{
    [SerializeField] private Sprite[] ButtonSprite;
    [SerializeField] private Image targetbutton;
    
    public void SpriteChange()
    {
        if(targetbutton.sprite == ButtonSprite[0])
        {
            targetbutton.sprite = ButtonSprite[1];
            return;
        }

        targetbutton.sprite = ButtonSprite[0];
    }
}
