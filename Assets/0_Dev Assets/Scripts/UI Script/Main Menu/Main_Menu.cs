using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Main_Menu : MonoBehaviour
{
    public void StartGame()
    {
        Debug.Log("Starting!");
        SceneManager.LoadScene("Game Manager");
    }

    public void Quit()
    {
        Debug.Log("Quit!");
        Application.Quit();
    }
}
