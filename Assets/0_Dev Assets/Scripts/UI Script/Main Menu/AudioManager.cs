using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    static AudioManager instance = null;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            GameObject.DontDestroyOnLoad(gameObject);
        }
    }
    
    public void ToggleSound()
    {
        if (PlayerPrefs.GetInt("SFXMuted", 0) == 0)
        {
            PlayerPrefs.SetInt("SFXMuted", 1);
            GetComponent<AudioSource>().volume = 0;
        }
        else
        {
            PlayerPrefs.SetInt("SFXMuted", 0);
            GetComponent<AudioSource>().volume = 1;
        }
    }
    /*private bool isMute;

    [Header("Sprite Setting")]
    [SerializeField] private Sprite[] ButtonSprite;
    [SerializeField] private Image targetbutton;

    private void Awake()
    {
        //Jd hanya butuh 1 music manager
        GameObject[] sfxobj = GameObject.FindGameObjectsWithTag("SFX");
        if (sfxobj.Length > 1)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);

    }
    private void Start()
    {
        isMute = PlayerPrefs.GetInt("SFXMuted") == 1;
        
    }

    private void Update()
    {
        if (isMute)
        {
            targetbutton.sprite = ButtonSprite[1];
        }
        else
        {
            targetbutton.sprite = ButtonSprite[0];
        }
    }

    public void MuteAudio()
    {
        isMute = !isMute;
        GetComponent<AudioSource>().mute = isMute;
        PlayerPrefs.SetInt("SFXMuted", isMute ? 1 : 0);

    }*/
}
