using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    [SerializeField] private SpriteState spriteState;
    [SerializeField] private Button button;

    public void ClickButton()
    {
        button.spriteState = spriteState;
    }
}
