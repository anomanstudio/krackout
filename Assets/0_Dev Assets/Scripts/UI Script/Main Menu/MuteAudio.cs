using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MuteAudio : MonoBehaviour
{
    bool isMute;
    
    public void MuteAll()
    {
        isMute = !isMute;
        AudioListener.volume = isMute ? 0 : 1;
    }
}
