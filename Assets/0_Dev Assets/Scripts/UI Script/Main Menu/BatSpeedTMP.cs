using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BatSpeedTMP : MonoBehaviour
{
    //Script Reference
    private PaddleMove_Test currentspeed;
    private PaddleStats speedstats;

    //UI
    public TextMeshProUGUI TextSpeed;
    public Button DecreaseSpeed;
    public Button IncreaseSpeed;

    //Numbers
    private float max = 90;
    private float min = 10;

    public float ConvertNumber;

    public float currentammount;
    private float increaseamount = 10;

    //Change Sprite
    [SerializeField] private Sprite[] _spriteBtn;
    [SerializeField] private Image targetbutton1;
    [SerializeField] private Image targetbutton2;

    private void Awake()
    {
        currentspeed = FindObjectOfType<PaddleMove_Test>();
        speedstats = FindObjectOfType<PaddleStats>();
        currentammount = speedstats.SpeedStat;
    }

    private void Update()
    {
        TextSpeed.text = ConvertNumber.ToString();
        speedstats.SpeedStat = currentammount;
        //Membuat angka menjadi lebih kecil tapi speed tetap kelipatan 10
        ConvertNumber = currentammount / 10;

        if (currentammount == min)
        {
            targetbutton1.sprite = _spriteBtn[1];
        }
        else
        {
            targetbutton1.sprite = _spriteBtn[0];
        }

        if (currentammount == max)
        {
            targetbutton2.sprite = _spriteBtn[3];
        }
        else
        {
            targetbutton2.sprite = _spriteBtn[2];
        }
    }

    public void adjustvalue(bool increase)
    {
        //Menghitung angka
        currentammount = Mathf.Clamp(currentammount + (increase ? increaseamount : -increaseamount), min, max);

        //Function button ketika mencapai batas
        DecreaseSpeed.interactable = currentammount > min;
        IncreaseSpeed.interactable = currentammount < max;
    }
}
