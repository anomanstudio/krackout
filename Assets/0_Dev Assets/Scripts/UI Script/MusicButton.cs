using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicButton : MonoBehaviour
{
    private MusicScript music;
    public Button musicToggleButton;
    public Sprite musicOnSprite;
    public Sprite musicOffSprite;
    //private gameObject Audio;

    void start()
    {
        //Audio.GetComponent.Find("AudioSource").;
        music = GameObject.FindObjectOfType<MusicScript>();
        UpdatedIconAndVolume();
    }

    public void PauseMusic()
    {
        FindObjectOfType<MusicScript>().ToggleSound();
        //music.ToggleSound();
        UpdatedIconAndVolume();
    }

    void UpdatedIconAndVolume()
    {
        if (PlayerPrefs.GetInt("MusicMuted",0) == 0)
        {
            
            musicToggleButton.GetComponent<Image>().sprite = musicOnSprite;

        }
        else
        {
            
            musicToggleButton.GetComponent<Image>().sprite = musicOffSprite;
        }
    }
}
