using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Submit : MonoBehaviour
{
    [SerializeField] private HighscoreTable highscoreTable;

    public string playername;
    public GameObject Inputfield;

    private void update()
    {

    }

    private void Awake()
    {
        
    }

    // Start is called before the first frame update
    public void SetScore()
    {
        playername = Inputfield.GetComponent<InputField>().text;
        highscoreTable.AddHighScore(Game_Manager.score, playername); //contoh
    }

    public void testing()
    {
        SceneManager.LoadScene("Leaderboardtemplate");
    }
}
