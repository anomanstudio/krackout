﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighscoreTable : MonoBehaviour {

    private Transform entryContainer;
    private Transform entryTemplate;
    //private List<HighScoreEntry> highscoreentrylist;
    private List<Transform> highscoreentrylistTransformList;

    private void Start()
    {
        entryContainer = transform.Find("LeaderBoardContainer");
        entryTemplate = entryContainer.Find("LeaderBoardElement");

        entryTemplate.gameObject.SetActive(false);

        //Contoh
        /*
        highscoreentrylist = new List<HighScoreEntry>()
        {
            new HighScoreEntry{ score = 100, name = "Anoman"},
            new HighScoreEntry{ score = 90, name = "Alsa"},
            new HighScoreEntry{ score = 80, name = "Ibyan"},
            new HighScoreEntry{ score = 40, name = "Okta"},
            new HighScoreEntry{ score = 50, name = "Zayn"},
        }; */

        //AddHighScore(3000, "Anoman");

        string jsonstring = PlayerPrefs.GetString("HighscoreTable");
        Highscores highscore = JsonUtility.FromJson<Highscores>(jsonstring);

        //Sort berdasarkan urutan terbesar
        for (int i = 0; i < highscore.highscoreentrylist.Count; i++)
        {
            for (int j = i + 1; j < highscore.highscoreentrylist.Count; j++)
            {
                if (highscore.highscoreentrylist[j].score > highscore.highscoreentrylist[i].score)
                {
                    //Swap posisi
                    HighScoreEntry tmp = highscore.highscoreentrylist[i];
                    highscore.highscoreentrylist[i] = highscore.highscoreentrylist[j];
                    highscore.highscoreentrylist[j] = tmp;
                }
            }
        }

        //Limit the entry to 5
        if (highscore.highscoreentrylist.Count > 5)
        {
            for (int h = highscore.highscoreentrylist.Count; h > 5; h--)
            {
                highscore.highscoreentrylist.RemoveAt(5);
            }
        }

        //Display list dari Create High Score Entry Transform
        highscoreentrylistTransformList = new List<Transform>();
        foreach (HighScoreEntry highscoreentry in highscore.highscoreentrylist)
        {
            CreateHighScoreEntryTransform(highscoreentry, entryContainer, highscoreentrylistTransformList);
        }

        /*
        //Playerprefs section
        //saving
        Highscores highscores = new Highscores { highscoreentrylist = highscoreentrylist };
        string json = JsonUtility.ToJson(highscores); //Highscore disimpan di format json

        //contoh
        PlayerPrefs.SetString("HighscoreTable", json);
        PlayerPrefs.Save();
        Debug.Log(PlayerPrefs.GetString("HighscoreTable"));
        */

        Debug.Log(PlayerPrefs.GetString("HighscoreTable"));
    }

    public void AddHighScore(int score, string name)
    {
        //Create entry
        HighScoreEntry highscoreentry = new HighScoreEntry { score = score, name = name };

        //Load saved entry
        string jsonstring = PlayerPrefs.GetString("HighscoreTable");
        Highscores highscore = JsonUtility.FromJson<Highscores>(jsonstring);

        //Limit the Score entry
        if (highscore != null && highscore.highscoreentrylist.Count > 7)
        {
            for (int h = highscore.highscoreentrylist.Count; h > 7; h--)
            {
                highscore.highscoreentrylist.RemoveAt(7);
            }
        }

        //Solve Null
        if (highscore == null)
        {
            var FirstEntry = new List<HighScoreEntry>() { highscoreentry }; //Dia akan membuat entry pertama
            highscore = new Highscores { highscoreentrylist = FirstEntry };
        }
        else
        {
            //Add new entry
            Debug.Log(highscoreentry);
            highscore.highscoreentrylist.Add(highscoreentry);
        }

        //Save Updated entry
        string json = JsonUtility.ToJson(highscore); //Highscore disimpan di format json
        PlayerPrefs.SetString("HighscoreTable", json);
        PlayerPrefs.Save();

        Debug.Log(PlayerPrefs.GetString("HighscoreTable"));
    }

    private void CreateHighScoreEntryTransform
        (HighScoreEntry highScoreentry, Transform Container, List<Transform> transformlist)
    {
        float templatehieght = 110f;

        //Buat template
        Transform entryTransform = Instantiate(entryTemplate, Container);
        RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();
        entryRectTransform.anchoredPosition = new Vector2(0, -templatehieght * transformlist.Count);
        entryTransform.gameObject.SetActive(true); //Set true untuk instantiated object

        //Sortir rank
        int rank = transformlist.Count + 1;
        string rankString;
        switch (rank)
        {
            default:
                rankString = rank + ""; break;
        }

        //untuk nomor
        entryTransform.Find("No").GetComponent<Text>().text = rankString; 

        //untuk nama
        string name = highScoreentry.name; //contoh
        entryTransform.Find("Name").GetComponent<Text>().text = name;

        //untuk score
        int score = highScoreentry.score; //contoh
        entryTransform.Find("Score").GetComponent<Text>().text = score.ToString();

        transformlist.Add(entryTransform);
    }

    //Tempat untuk save
    private class Highscores
    {
        public List<HighScoreEntry> highscoreentrylist;
    }

    //Represent a single highscore
    [System.Serializable]
    private class HighScoreEntry
    {
        public int score;
        public string name;
    }
}
