using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioButton : MonoBehaviour
{
    private AudioManager audiomanager;
    public Button audioToggleButton;
    public Sprite audioOnSprite;
    public Sprite audioOffSprite;

    void start()
    {
        audiomanager = GameObject.FindObjectOfType<AudioManager>();
        UpdatedIconAndVolume();
    }

    public void PauseAudio()
    {
        FindObjectOfType<AudioManager>().ToggleSound();
        //music.ToggleSound();
        UpdatedIconAndVolume();
    }

    void UpdatedIconAndVolume()
    {
        if (PlayerPrefs.GetInt("SFXMuted",0) == 0)
        {
            
            audioToggleButton.GetComponent<Image>().sprite = audioOnSprite;

        }
        else
        {
            
            audioToggleButton.GetComponent<Image>().sprite = audioOffSprite;
        }
    }
}
