using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MusicScript : MonoBehaviour
{
    static MusicScript instance = null;
    public AudioClip[] songs;
    int currentsongs = 0;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            GameObject.DontDestroyOnLoad(gameObject);
        }
    }

    private void Update()
    {
        //Untuk play music di playlist
        if (GetComponent<AudioSource>().isPlaying == false)
        {
            currentsongs++;
            if (currentsongs >= songs.Length)
            {
                currentsongs = 0;
            }
            GetComponent<AudioSource>().clip = songs[currentsongs];
            GetComponent<AudioSource>().Play();
        }


    }

    public void ToggleSound()
    {
        if (PlayerPrefs.GetInt("MusicMuted", 0) == 0)
        {
            PlayerPrefs.SetInt("MusicMuted", 1);
            GetComponent<AudioSource>().volume = 0;
        }
        else
        {
            PlayerPrefs.SetInt("MusicMuted", 0);
            GetComponent<AudioSource>().volume = 0.17f;
        }
    }
    /*bool isMute;

    [Header("Audio Clips")]
    public AudioClip[] songs;
    int currentsongs = 0;

    [Header("Sprite Setting")]
    [SerializeField] private Sprite[] ButtonSprite;
    [SerializeField] private Image targetbutton;

    private void Awake()
    {
        //Jd hanya butuh 1 music manager
        GameObject[] musicobj = GameObject.FindGameObjectsWithTag("GameMusic");
        if(musicobj.Length > 1)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);

        
    }

    private void Start()
    {
        isMute = PlayerPrefs.GetInt("MusicMuted") == 0;
        //GetComponent<AudioSource>().mute = isMute;
    }

    private void Update()
    {
        //Untuk play music di playlist
        if (GetComponent<AudioSource>().isPlaying == false)
        {
            currentsongs++;
            if(currentsongs >= songs.Length)
            {
                currentsongs = 0;
            }
            GetComponent<AudioSource>().clip = songs[currentsongs];
            GetComponent<AudioSource>().Play();
        }

        
    }

    public void MuteMusic()
    {
        isMute = !isMute;
        GetComponent<AudioSource>().mute = isMute; //Mute akan dicheck apabila kondisi isMute = true
        PlayerPrefs.SetInt("MusicMuted", isMute ? 1 : 0);
    }

    public void SpriteChange()
    {
        if (targetbutton.sprite == ButtonSprite[0])
        {
            targetbutton.sprite = ButtonSprite[1];
            return;
        }

        targetbutton.sprite = ButtonSprite[0];
    }*/


}
