using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIButton : MonoBehaviour
{
    //Script References
    private Game_Manager Game;

    //Button Function
    public void playNewGame()
    {
        FindObjectOfType<Game_Manager>().NewGame();
        FindObjectOfType<Timer>().ResetTime();
    }

    public void mainMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }

    public void startGame()
    {
        SceneManager.LoadScene("Game Manager");
        Debug.Log("Starting!");
    }

    public void ToNextLevel()
    {
        Time.timeScale = 1f;
        Game_Manager.level += 1;
        SceneManager.LoadScene("Level " + Game_Manager.level);
        FindObjectOfType<PowerUpChecker>().disableAll();
        FindObjectOfType<Timer>().ResetTimer();
    }

    public void Quit()
    {
        Application.Quit();
        Debug.Log("Quitting");
    }
}
