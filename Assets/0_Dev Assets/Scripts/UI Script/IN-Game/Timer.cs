using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public Text timerText;
    private float startTime;
    private bool TimerStart = true;
    
    void Start()
    {
        startTime += Time.deltaTime;
        stopTime();
    }
    private void Awake()
    {
        
    }


    void Update()
    {
        if (TimerStart)
        {
            float t = Time.timeSinceLevelLoad - startTime;

            string minutes = ((int)t / 60).ToString("f2");
            string seconds = (t % 60).ToString("f0");

            timerText.text = minutes + ":" + seconds;
        }
    }

    public void ResetTimer()
    {  
        {
            startTime = 0;
        }
    }

    public void Finnish()
    {
        TimerStart = !TimerStart;
    }

    public void ResetTime()
    {
        startTime = Time.timeSinceLevelLoad;
    }

    public void stopTime()
    {
        Time.timeScale = 0f;
    }

    public void resumeTime()
    {
        Time.timeScale = 1f;
    }
}
