using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpText : MonoBehaviour
{
    private PowerUpChecker CurrentPowerUp;
    public Text _textPowerUp;

    private void Awake()
    {
        CurrentPowerUp = FindObjectOfType<PowerUpChecker>();
    }

    private void Update()
    {
        if(CurrentPowerUp._missileActive == true)
        {
            _textPowerUp.text = "MISSILE"; 
        }
        else if (CurrentPowerUp._glueActive == true)
        {
            _textPowerUp.text = "GLUE";
        }
        else if (CurrentPowerUp._expandActive == true)
        {
            _textPowerUp.text = "EXPAND";
        }
        else if (CurrentPowerUp._multiplierActive == true)
        {
            _textPowerUp.text = "MULTIPLIER";
        }
        else if (CurrentPowerUp._Slowdownactive == true)
        {
            _textPowerUp.text = "SLOW DOWN";
        } 
        else if (CurrentPowerUp._ShieldActive == true)
        {
            _textPowerUp.text = "SHIELD";
        }
        else
        {
            _textPowerUp.text = " ";
        }
    }
}
