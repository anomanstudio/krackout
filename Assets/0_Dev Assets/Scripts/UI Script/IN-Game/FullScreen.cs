using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FullScreen : MonoBehaviour
{
    [SerializeField] private Sprite[] SliderSprite;
    [SerializeField] private Image TargetSlider;
    public Slider slider;
    public float sliderValue;

    private void Awake()
    {
        //slider.value = 2;
    }
    private void Start()
    {
        if(Screen.fullScreenMode == FullScreenMode.Windowed)
        {
            slider.value = 1;
        }
        else
        {
            slider.value = 2;
        }
    }
    private void Update()
    {
        if (slider.value == 2f)
        {
            TargetSlider.sprite = SliderSprite[1];
            Fullscene(true);
        }
        else if (slider.value == 1f)
        {
            TargetSlider.sprite = SliderSprite[0];
            Windowed(true);
        }
    }

    public void Fullscene(bool is_fullscreen)
    {
        Screen.fullScreen = is_fullscreen;
        //Debug.Log("Game is Full Screen");
    }

    public void Windowed(bool is_Windowed)
    {
        Screen.fullScreenMode = FullScreenMode.Windowed;
        //Debug.Log("Game is Not Full screen");
    }
}
