using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
    [SerializeField] GameObject pauseMenu;
    [SerializeField] GameObject pauseUI;

    void Start()
    {
        pauseUI = this.transform.parent.Find("Paused").gameObject;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && Time.timeScale == 1f)
        {
            pause();
            pauseUI.SetActive(true);
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && Time.timeScale == 0f)
        {
            resume();
            pauseUI.SetActive(false);
        }
    }

    public void pause()
    {
        Time.timeScale = 0f;
    }

    public void resume()
    {   
        Time.timeScale = 1f;
    }
}
