using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public Text scoreText;
    public Text livesText;
    public Text levelText;
       void Update() 
    {
        //score = Game_Manager.score;
        scoreText.text = Game_Manager.score.ToString("0");
        livesText.text = Game_Manager.lives.ToString("0");
        levelText.text = Game_Manager.level.ToString("0");
    }
    public int score{
        get {
            return score;
        }

        set {
            score = value;
        }
    }
}
