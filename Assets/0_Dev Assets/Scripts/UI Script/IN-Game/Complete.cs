using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Complete : MonoBehaviour
{
    public GameObject CompletePanel;

    public void CompleteLevel()
    {
        CompletePanel.SetActive(true);
        FindObjectOfType<PowerUpChecker>().disableAll();
        FindObjectOfType<Ball_Bounce>().ResetBall();
        FindObjectOfType<PaddleMove_Test>().ResetPaddle();
    }
}
